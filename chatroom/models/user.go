package models

import (
	"github.com/astaxie/beego/orm"
)

type User struct {
	Id       int64  `json:"id" form:"-"`
	Username string `json:"username" form:"username"` //用户名
	Password string `json:"password" form:"password"` //密码
	Islogin  bool   `json:"islogin" form:"islogin"`   //是否离线
}

/**添加用户基本信息*/
func AddUser(user *User) bool {
	o := orm.NewOrm()
	count, err := o.Insert(user)
	if count > 0 && err == nil {
		return true
	} else {
		return false
	}
}

/**通过用户信息获取用户*/
func GetUserByUser(user *User) User {
	o := orm.NewOrm()
	var dbUser User
	o.QueryTable("user").Filter("username", user.Username).Filter("password", user.Password).One(&dbUser, "id", "username")
	return dbUser
}

/**通过用户名获取用户基本信息*/
func GetUserByName(username string) User {
	o := orm.NewOrm()
	var dbUser User
	o.QueryTable("user").Filter("username", username).One(&dbUser, "id", "username", "islogin")
	return dbUser
}

/**获取所有的用户列表*/
func List() []User {
	var dbUsers []User
	o := orm.NewOrm()
	o.QueryTable("user").Filter("islogin", 1).All(&dbUsers, "id", "username", "islogin")
	return dbUsers
}

/**设置用户的登录状态*/
func SetLoginState(user *User) {
	o := orm.NewOrm()
	o.Update(user, "islogin")
}

/**服务器启动重置所有用户的状态*/
func ResetState() {
	o := orm.NewOrm()
	o.Raw("update user set islogin = ? where 1=1", 0).Exec()
}

/**注册模型*/
func init() {
	orm.RegisterModel(new(User))
}
