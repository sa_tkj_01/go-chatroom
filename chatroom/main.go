package main

import (
	"chatroom/models"
	_ "chatroom/routers"
	"github.com/astaxie/beego"
)

func init() {
	models.ResetState()
}

func main() {
	beego.Run()
}
