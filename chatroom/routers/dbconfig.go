package routers

import (
	_ "chatroom/models"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

func init() {
	mysqlurl := beego.AppConfig.String("mysqlurl")
	orm.RegisterDriver("mysql", orm.DRMySQL)
	orm.RegisterDataBase("default", "mysql", mysqlurl, 30, 30)
	orm.RunSyncdb("default", false, true)
}
