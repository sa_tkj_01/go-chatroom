package routers

import (
	"chatroom/controllers"
	"chatroom/filter"
	"github.com/astaxie/beego"
)

func init() {
	/**配置静态资源路径*/
	beego.SetStaticPath("/static", "static")
	/**配置路由信息*/
	beego.Router("/", &controllers.MainController{})
	beego.AutoRouter(&controllers.UserController{}) //自动路由
	beego.Router("/ws", &controllers.UserController{}, "*:RegisterClient")
	filter.RunLoginFilter()
}
